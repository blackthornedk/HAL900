#pragma once

#include <stdint.h>

#define MAX_BUFFER 254

void rs485Init(void);
void handleReceivedBuffer(void);
