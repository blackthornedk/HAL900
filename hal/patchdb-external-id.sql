
begin;
drop table external_validation_batch_id;
commit;

begin;
drop table external_validation_batch;
commit;

begin;
drop table external_member_id;
commit;

begin;
drop table external_organization;
commit;

begin;

create table external_organization (
                                       id varchar(10) primary key,
                                       name varchar not null unique,
                                       description varchar not null,
                                       validation_email varchar not null,
                                       name_of_external_id varchar not null
);

create table external_member_id (
                                    external_organization_id varchar(10) references external_organization(id),
                                    member_id integer references member(id),
                                    value varchar not null,
                                    created timestamp default now() not null,

                                    primary key (external_organization_id, value)
);

insert into external_organization (id, name, description, validation_email, name_of_external_id)
values ('prosa', 'PROSA',
        'PROSA tilbyder at give deres medlemmer et tilskud til OSAA-kontingentet på 50% af det normale OSAA-kontingent. Som en betingelse for tilskuddet, skal OSAA en gang om måneden indsende oplysninger om hvilke PROSA-medlemmer man ønsker tilskuddet udbetalt for. OSAA deler derfor dine personlige oplysninger med PROSA en gang om måneden.',
        'hja@prosa.dk',
        'PROSA medlemsnummer');

create table external_validation_batch (
                                           id serial primary key not null,
                                           name varchar unique not null,
                                           external_organization_id varchar(10) references external_organization(id) not null,
                                           created timestamp default now() not null,
                                           validation_token varchar unique not null,
                                           validated timestamp
);

create table external_validation_batch_id (
                                              member_id integer references member(id) not null,
                                              external_membership_batch_id integer references external_validation_batch(id) not null,
                                              value varchar not null,
                                              amount numeric(10,2) not null,

                                              unique (external_membership_batch_id, value),
                                              primary key (external_membership_batch_id, member_id)
);

commit;

begin;
create table external_admin
(
    external_organization_id varchar(10) references external_organization (id),
    member_id                integer references member (id),
    created_time             timestamp default now() not null,
    primary key (external_organization_id, member_id)
);

create table external_member_log
(
    external_organization_id varchar(10) references external_organization (id),
    member_id                integer references member (id),
    external_admin_id        integer references member (id),
    created_date             date default now() not null,
    approved_time            timestamp,
    approved                 boolean,
    primary key (external_organization_id, member_id, created_date)
);

insert into membertype (id, membertype, monthlyfee, dooraccess, userpick) values (11, 'Extern administrator', 0, false, false);
commit;

begin;

drop table external_validation_batch;
drop table external_validation_batch_id;

commit;

begin;

create table external_membership_sponsorship
(
    external_organization_id varchar(10) references external_organization (id),
    membertype_id int references membertype (id),
    monthly_sponsorship numeric(10,2),
    primary key (external_organization_id, membertype_id)
);

create table external_member_log_transaction
(
    external_organization_id varchar(10) references external_organization (id),
    member_id                integer references member (id),
    external_admin_id        integer references member (id),
    created_date             timestamp default now() not null,
    approved_time            timestamp,
    approved                 boolean,
    primary key (external_organization_id, member_id, created_date, approved_time)
);

insert into external_membership_sponsorship (external_organization_id, membertype_id, monthly_sponsorship)
    values
        ('prosa', 1, 100), -- Legacy membership
        ('prosa', 4, 50), -- Legacy student membership
        ('prosa', 7, 118), -- Standard membership
        ('prosa', 8, 56), -- Student membership
        ('prosa', 9, 118), -- Ultra membership
        ('prosa', 10, 118)  -- 🧐 membership
;

commit;

begin;
alter table external_organization
    add column account_id int default null,
    add foreign key (account_id) references account (id)
;

update external_organization set account_id = 405 where id = 'prosa';
commit;
