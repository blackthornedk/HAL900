# Using docker-compose

* Create your .env file with: ./create-docker-compose-env
* Run docker-compose build && docker-compose up -d
* Run sudo ./restart-apache-container-when-needed

The last command runs a loop that stats the perl files and rebuilds and restarts the apache container when they change.

The .env file contains variables that are interpolated into the docker-compose.yaml file as documented here:
https://docs.docker.com/compose/environment-variables/set-environment-variables/

The default location to store the state of the dockerized setup is the docker subdirectory, it's ignored by
the restart-apache-container-when-needed script

# Using docker-compose in WSL

When using WSL and docker-compose, you need to add a few steps.
First off, use another 'web_port' than 80, since Windows IP Service tends to take up port 80.
Web and DB ports have been added to the env file and scripts.

Secondly the folders for the volumes need to be created by hand. The 'create-docker-compose-env' script should have
created a 'docker' folder inside the hal folder. Inside this you need to create a 'db' and a 'logs' folder, and set
their permissions to 777. The bitnami postgresql container will use UID 1001 for the data folder inside 'db', 
regardless of what that UID is used for in your current WSL environment. This is not an error, and is due to the 
postgresql container running as a rootless container.

The extra commands have been added to the `create-docker-compose-env` script, and can be invoked by adding the 'WSL' 
argument to the script.

The WSL setup have been tested, using rancher-desktop integrated in WSL, but should also work with docker-desktop.

# Restore a backup

PGPASSWORD=password123 pg_restore -h localhost -U hal -d hal /tmp/hal.Sunday-13.pg

PGPASSWORD=password123 pg_restore -c -h 172.19.0.2 -U hal -d hal /home/ff/projects/HAL900/hal-backups/hal.Wednesday-06.pg

# psql

PGPASSWORD=password123 psql -h 172.19.0.2 -U hal -d hal
