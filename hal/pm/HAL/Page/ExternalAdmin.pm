#-*-perl-*-
package HAL::Page::ExternalAdmin;
use strict;
use warnings;
use utf8;

use Apache2::Const;
use Data::Dumper;
use HTML::Entities;
use Digest::SHA qw(sha1_hex);
use POSIX;
use Date::Calc 'Days_in_Month';

use HAL;
use HAL::Pages;
use HAL::Session;
use HAL::Util;
use HAL::Email;
use HAL::Layout;

sub indexPage {
    my ($r, $q, $p) = @_;

    my $html = '';

    $html .= qq'<p>';
    $html .= qq'<span><a href="/hal/extern-admin/">Externe Organisationer</a></span>';
    $html .= qq'</p>';

    my $orgResult;
    if (isAdmin) {
        $orgResult = db->sql('select org.id, org.name from external_organization org') or die "Failed to fetch list of memberships for admin";
    } else {
        $orgResult = db->sql('select org.id, org.name from external_organization org join external_admin adm on ' .
            '(org.id=adm.external_organization_id) where adm.member_id=?',
            getSession->{member_id}) or die "Failed to fetch list of memberships for admin";
    }

    my @orgs;
    while (my ($orgId, $orgName) = $orgResult->fetchrow_array) {
        push @orgs, {
            id   => $orgId,
            name => $orgName,
        };
    }
    $orgResult->finish;

    $html .= "<h2>Ekstern administrator</h2><table><tr><th>Organisation</th></tr>\n";
    if (@orgs) {
        for my $org (@orgs) {
            $html .= qq'<tr><td><a href="/hal/extern-admin/$org->{id}/months/">$org->{name}</a></td><tr>';
        }
    } else {
        $html .= "<tr><td colspan=2>Der er ingen eksterne organisationer tilknyttet denne administrator</td><tr>";
    }
    $html .= "</table>";

    return outputFrontPage('extern-admin', 'Ekstern Administrator Oversigt', $html);
}

sub monthsListPage {
    my ($r, $q, $p, $orgId) = @_;

    my $html = '';

    my $orgResult = db->sql('select name, description, name_of_external_id from external_organization where id = ?', $orgId)
        or die "Failed to fetch info for organization";

    my ($orgName, $orgDescription, $orgExternIdName) = $orgResult->fetchrow_array;
    $orgResult->finish;

    $html .= qq'<p>';
    $html .= qq'<span><a href="/hal/extern-admin/">Externe Organisationer</a></span>';
    $html .= qq' &gt; <span><a href="/hal/extern-admin/$orgId/months/">$orgName</a></span>';
    $html .= qq'</p>';

    $html .= "<h2>Ekstern administrator for $orgName</h2>\n";
    $html .= "<p>$orgDescription</p>\n";

    $html .= "<h3>Måneder med medlemsstatus:</h3>\n";

    my $monthListResult = db->sql('select date_part(\'year\', created_date) as year, date_part(\'month\', created_date) as month ' .
        'from external_member_log where external_organization_id = ? group by date_part(\'year\', created_date), date_part(\'month\', created_date) ' .
        'order by date_part(\'year\', created_date), date_part(\'month\', created_date)', $orgId)
        or die "Failed to fetch months for External Organization";

    my @months = ('Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'December');

    my $reimburseHeader = '';
    if (isAdmin) {
        $reimburseHeader = '<th>Antal refunderede medlemmer</th>';
    }

    $html .= qq'<table><thead><tr><th>Måned</th><th>Antal betalende medlemmer</th><th>Antal godkendte medlemmer</th>$reimburseHeader</tr></thead><tbody>';
    while (my ($userYear, $userMonth) = $monthListResult->fetchrow_array) {
        my $comment = getMonthTransactionComment($userYear, $userMonth);

        my $fromPart = 'from external_member_log eml ' .
            'left join account act on (eml.member_id = act.owner_id and act.type_id = 2) ' .
            'left join accounttransaction act_trans on (act.id = act_trans.source_account_id and act_trans.comment = ?)';
        my $wherePart = 'where eml.external_organization_id = ? and date_part(\'year\', eml.created_date) = ? ' .
            'and date_part(\'month\', eml.created_date) = ?';

        my $memberCountResult = db->sql(qq'select count(eml.member_id) as members $fromPart $wherePart', $comment, $orgId, $userYear, $userMonth)
            or die "Failed to fetch member count status for External Organization";
        my $approvedCountResult = db->sql(qq'select count(eml.member_id) as approved $fromPart $wherePart and eml.approved = True', $comment, $orgId, $userYear, $userMonth)
            or die "Failed to fetch approved count status for External Organization";
        my $paidCountResult = db->sql(qq'select count(act_trans.id) as paid $fromPart $wherePart', $comment, $orgId, $userYear, $userMonth)
            or die "Failed to fetch member count status for External Organization";

        my $memberCount = $memberCountResult->fetchrow_array;
        my $approvedCount = $approvedCountResult->fetchrow_array;
        my $paidCount = $paidCountResult->fetchrow_array;

        my $date = sprintf("%4d-%02d", $userYear, $userMonth);
        $html .= qq'<tr>\n';
        $html .= qq'<td><a href="/hal/extern-admin/$orgId/months/$date/">$months[$userMonth - 1] $userYear</a></td>\n';
        $html .= qq'<td>$memberCount</td>';
        $html .= qq'<td>$approvedCount</td>';
        if (isAdmin) {
            $html .= qq'<td>$paidCount</td>';
        }
        $html .= qq'</tr>\n';
    }
    $monthListResult->finish;
    $html .= qq'</tbody></table>\n';

    return outputFrontPage('extern-admin', "Ekstern administrator for $orgName", $html);
}

sub monthsPage {
    my ($r, $q, $p, $orgId, $year, $month, $format) = @_;

    my $csv = $format =~ /csv/ ? 1 : 0;

    if ($p->{gogogo}) {
        monthsPagePost($r, $q, $p, $orgId, $year, $month);
        return;
    }

    if (isAdmin) {
        foreach my $key (keys %$p) {
            my $reimburse = $key =~ /reimburse_[0-9]?/ ? 1 : 0;
            if ($reimburse == 1) {
                monthsPageReimburse($r, $q, $p, $orgId, $year, $month);
                return;
            }
        }
    }

    my $html = '';
    my $content = '';

    my $orgResult = db->sql('select name, description, name_of_external_id from external_organization where id = ?', $orgId)
        or die "Failed to fetch info for organization";

    my ($orgName, $orgDescription, $orgExternIdName) = $orgResult->fetchrow_array;
    $orgResult->finish;

    my @months = ('Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'December');
    my $date = sprintf("%4d-%02d", $year, $month);

    $html .= qq'<p>';
    $html .= qq'<span><a href="/hal/extern-admin/">Externe Organisationer</a></span>';
    $html .= qq' &gt; <span><a href="/hal/extern-admin/$orgId/months/">$orgName</a></span>';
    $html .= qq' &gt; <span><a href="/hal/extern-admin/$orgId/months/$date/">$months[$month - 1] $year</a></span>';
    $html .= qq'</p>';

    $html .= qq'<h2>Ekstern administrator for $orgName</h2>\n';
    $html .= qq'<p>$orgDescription</p>\n';

    $html .= qq'<h3>Brugerstatus for $months[$month - 1] $year:</h3>\n';

    my $comment = getMonthTransactionComment($year, $month);
    if (isAdmin) {
        $html .= qq'<h4>Titel på transaktion: $comment</h4>';
    }

    $html .= qq'<div><a href="/hal/extern-admin/$orgId/months/$year-$month.csv">Download til Excel (CSV)</a>\n<br /><br />\n';

    $html .= qq'<form action="/hal/extern-admin/$orgId/months/$year-$month/" method="POST">\n';
    my $totalSponsorship = 0;
    my $totalReimbursed = 0;

    my $monthResult = db->sql('select emi.value, m.realname, eml.member_id, mt.membertype, ems.monthly_sponsorship, act_trans.amount ' .
        'from external_member_log eml ' .
        'join external_member_id emi on (eml.member_id = emi.member_id and eml.external_organization_id = emi.external_organization_id) ' .
        'join member m on (eml.member_id = m.id) ' .
        'join membertype mt on (m.membertype_id = mt.id) ' .
        'join external_membership_sponsorship ems on (eml.external_organization_id = ems.external_organization_id and m.membertype_id = ems.membertype_id)' .
        'left join account act on (emi.member_id = act.owner_id and act.type_id = 2)' .
        'left join accounttransaction act_trans on (act.id = act_trans.source_account_id and act_trans.comment = ?) ' .
        'where eml.external_organization_id = ? and date_part(\'year\', eml.created_date) = ? ' .
        'and date_part(\'month\', eml.created_date) = ? order by eml.member_id', $comment, $orgId, $year, $month)
        or die "Failed to fetch months for External Organization";

    my $actionHeader = '&nbsp;';
    if (isAdmin) {
        $actionHeader = 'Refusion';
    }

    my $count = 0;
    $html .= qq'<table><thead><tr><th>$orgExternIdName</th><th>Navn</th><th>Kontingent&shy;tilskud</th>';
    $html .= qq'<th>Dato</th><th>Godkender</th><th>Status</th><th>$actionHeader</th></tr></thead><tbody>';

    $content .= qq'$orgExternIdName;Navn;Medlemstype;Dato;Godkender;Status\n';

    while (my ($memberId, $memberName, $id, $memberType, $sponsorship, $paidAmount) = $monthResult->fetchrow_array) {
        $totalSponsorship += $sponsorship;
        my $memberStateQuery = "select adm.realname, to_char(eml.approved_time, 'YYYY-MM-DD HH24:MI'), eml.approved " .
            "from external_member_log eml " .
            "join member adm on (eml.external_admin_id = adm.id) " .
            "where eml.external_organization_id = ? and eml.member_id = ? " .
            "and date_part('month', eml.created_date) = ? and date_part('year', eml.created_date) = ? " .
            "order by eml.created_date desc limit 1";
        my $approvalResult = db->sql($memberStateQuery, $orgId, $id, $month, $year)
            or die "Failed to fetch approval status for member";

        my ($adminName, $approvedTime, $approved) = $approvalResult->fetchrow_array;
        $approvalResult->finish;
        my $approvalState = 'afventer';
        if (defined $approved) {
            if (defined $approved && $approved) {
                $approvalState = qq'godkendt';
            } else {
                $approvalState = qq'afvist';
            }
        }

        my $approvalLink = '';

        if (isAdmin) {
            if (defined $approved && $approved) {
                if (defined $paidAmount) {
                    $approvalLink = qq'$paidAmount';
                    $totalReimbursed += $paidAmount;
                } else {
                    $approvalLink = "<fieldset><legend>Udbetal refusion</legend>";
                    $approvalLink .= qq'<div>';
                    $approvalLink .= qq'<input type="submit" id="reimburse_$id" name="reimburse_$id" value="Betal" />';
                    $approvalLink .= qq'</div>';

                    $approvalLink .= "</fieldset>";
                }
            } else {
                $approvalLink = qq'Ikke godkendt';
            }
        } else {
            $approvalLink = "<fieldset><legend>Godkend medlem</legend>";
            {
                my $checked = $approvalState eq 'godkendt' ? 'checked ' : '';
                $approvalLink .= qq'<div>';
                $approvalLink .= qq'<input type="radio" id="approved_$id" name="approve_$id" value="approve" $checked/>';
                $approvalLink .= qq'<label for="approved_$id">Godkend</label>';
                $approvalLink .= qq'</div>';
            }

            {
                my $checked = $approvalState eq 'afvist' ? 'checked ' : '';
                $approvalLink .= qq'<div>';
                $approvalLink .= qq'<input type="radio" id="rejected_$id" name="approve_$id" value="reject" $checked/>';
                $approvalLink .= qq'<label for="rejected_$id">Afvis</label>';
                $approvalLink .= qq'</div>';
            }

            $approvalLink .= "</fieldset>";
        }

        if (!defined $adminName) {
            $adminName = 'N/A';
        }
        if (!defined $approvedTime) {
            $approvedTime = 'N/A';
        }
        my $sponsorshipStr = sprintf('%0.2f', $sponsorship);

        my $class = ($count++ & 1) ? 'class="odd"' : 'class="even"';
        $html .= qq'<tr $class><td>$memberId</td><td>$memberName</td><td align="right">$sponsorshipStr</td>';
        $html .= qq'<td>$approvedTime</td><td>$adminName</td><td>$approvalState</td><td>$approvalLink</td></tr>\n';

        if (defined $approved && $approved) {
            $content .= qq'$memberId;$memberName;$sponsorshipStr;$approvedTime;$adminName;$approvalState\n';
        }
    }
    my $totalSponsorshipStr = sprintf('%0.2f', $totalSponsorship);
    my $totalReimbursedStr = sprintf('%0.2f', $totalReimbursed);
    $html .= qq'</tbody><tfoot>';
    $html .= qq'<tr><th>Total</th><td>&nbsp;</td><td align="right">$totalSponsorshipStr</td><td colspan="4">&nbsp;</td></tr>';
    if (isAdmin) {
        $html .= qq'<tr><th>Total Refusion</th><td>&nbsp;</td><td align="right">$totalReimbursedStr</td><td colspan="4">&nbsp;</td></tr>';
    }
    $html .= qq'</tfoot></table>\n';
    $html .= qq'<br><input type="submit" name="gogogo" value="Gem"/></form>';
    $monthResult->finish;

    if ($csv) {
        return outputRaw('text/csv', $content, "osaa-medlemmer.$orgId.$year-$month.csv");
    }
    return outputFrontPage('extern-admin', qq'Ekstern administrator for $orgName', $html);
}

sub monthsPagePost {
    my ($r, $q, $p, $orgId, $year, $month) = @_;

    my $result = db->sql('select eml.member_id, eml.created_date ' .
        'from external_member_log eml ' .
        'join external_member_id emi on (eml.member_id = emi.member_id and eml.external_organization_id = emi.external_organization_id) ' .
        'where eml.external_organization_id = ? and date_part(\'year\', eml.created_date) = ? ' .
        'and date_part(\'month\', eml.created_date) = ? order by eml.member_id', $orgId, $year, $month)
        or die "Failed to fetch logs for External Members";

    while (my ($id, $createdDate) = $result->fetchrow_array) {
        my $approved = $p->{"approve_" . $id} or next;

        if ($approved eq 'approve' or $approved eq 'reject') {
            my $approvedBool = $approved eq 'approve' ? 'True' : 'False';
            db->sql('update external_member_log set external_admin_id = ?, approved_time = now(), approved = ? where external_organization_id = ? and member_id = ? and created_date = ?',
                getSession->{member_id}, $approvedBool, $orgId, $id, $createdDate) or die "Failed to update External Member Log";

            db->sql('insert into external_member_log_transaction (external_organization_id, member_id, external_admin_id, created_date, approved_time, approved) values (?, ?, ?, ?, now(), ?)',
                $orgId, $id, getSession->{member_id}, $createdDate, $approvedBool) or die "Failed to update External Member Transaction Log";
        }
    }
    $result->finish;

    $r->headers_out->set(Location => qq'/hal/extern-admin/$orgId/months/$year-$month/');
    $r->status(Apache2::Const::REDIRECT);
    return;
}

sub monthsPageReimburse {
    my ($r, $q, $p, $orgId, $year, $month) = @_;

    my $result = db->sql('select eml.member_id, eo.account_id, ems.monthly_sponsorship ' .
        'from external_member_log eml ' .
        'join external_member_id emi on (eml.member_id = emi.member_id and eml.external_organization_id = emi.external_organization_id) ' .
        'join member m on (eml.member_id = m.id) ' .
        'join membertype mt on (m.membertype_id = mt.id) ' .
        'join external_organization eo on (eml.external_organization_id = eo.id) ' .
        'join external_membership_sponsorship ems on (ems.external_organization_id = eml.external_organization_id and ems.membertype_id = m.membertype_id) ' .
        'where eml.external_organization_id = ? and date_part(\'year\', eml.created_date) = ? ' .
        'and date_part(\'month\', eml.created_date) = ? order by eml.member_id', $orgId, $year, $month)
        or die "Failed to fetch logs for External Members";

    while (my ($id, $orgAccountId, $monthlySponsorship) = $result->fetchrow_array) {
        my $reimburse = $p->{"reimburse_" . $id};

        if (defined $reimburse) {
            my $comment = getMonthTransactionComment($year, $month);

            my $ar = db->sql("select id from account where owner_id=? and type_id=2", $id);
            my ($sourceAccount) = $ar->fetchrow_array;
            $ar->finish;

            unless (defined $sourceAccount) {
                my $dr = db->sql("select realname from member where id=?", $id);
                my ($name) = $dr->fetchrow_array or die "Invalid member id: $id";
                $dr->finish;

                db->sql('insert into account (owner_id, type_id, accountName) values (?,2,?)',
                    $id, $name) or die "Failed to store the new account";
                $sourceAccount = db->getID('account') or die "Failed to get new account id";
                l "Created account $sourceAccount for $id type: 2";
            }

            my @accounts = ($sourceAccount, $orgAccountId);

            db->sql('insert into accountTransaction (source_account_id, target_account_id, amount, comment, operator_id) values (?, ?, ?, ?, ?)',
                @accounts, $monthlySponsorship, $comment, getSession->{member_id}
            ) or die "Failed to insert transaction " . join(',', @accounts, $monthlySponsorship, $comment);
        }
    }
    $result->finish;

    $r->headers_out->set(Location => qq'/hal/extern-admin/$orgId/months/$year-$month/');
    $r->status(Apache2::Const::REDIRECT);
    return;
}

sub getMonthTransactionComment() {
    my ($year, $month) = @_;
    my $sMonth = abs($month);
    my $lastDay = Days_in_Month($year, $sMonth);

    my $comment = qq'Kontingenttilskud $year 1/$sMonth-$lastDay/$sMonth';
    return $comment;
}

BEGIN {
    ensureExternAdmin(qr'^/hal/extern-admin');
    addHandler(qr'^/hal/extern-admin/?$', \&indexPage);
    addHandler(qr'^/hal/extern-admin/(\w+)/months/?$', \&monthsListPage);
    addHandler(qr'^/hal/extern-admin/(\w+)/months/(\d{4})-(\d{2})(\.csv)?/?$', \&monthsPage);
}

12;
